# API 
This API is provided to interact the backend database:
- to retrieve all dog pictures, 
- to retrieve a dog picture, 
- to retrieve all pictures by breed, 
- to retrieve the details of a particular dog, 
- to vote (UP or DOWN) to a dog picture, 
- to query the number of votes of a picture, 
- to query the numbers of votes grouped by the dog picture, 
- to query the numbers of votes grouped by the dog picture for certain minimum value of votes, 


# URI design
1. List all of the available dog pictures grouped by breed:
http://localhost:8080/pictures/all

2. List all of the available dog pictures of a particular breed:
http://localhost:8080/pictures/{breed}
e.g. http://localhost:8080/pictures/Labrador

3. Vote (POST) up and down a dog picture:
http://localhost:8080/vote/dog
(Data: "client_name", "dog_pic_id", "vote". 
e.g. {"client_name":"alan", "dog_pic_id":1, "vote":true}
Note: one client can only vote once for any dog picture.
)

4. The details associated with a dog picture:
http://localhost:8080/details/{dog_id}

5. The number of votes a picture has received:
http://localhost:8080/vote/{dog_pic_id}

6. The Dog Breed App expects the response to be sorted by the number of votes a picture has received:
http://localhost:8080/all_votes

7. Group the number of votes by dog picture for all votes greater than {10}:
http://localhost:8080/votes/10

8. Query the dog's breed:
http://localhost:8080/breed/{dog_pic_id}
 
Note:
The domain name will be replaced by the appropriate name in the production environment.

