package com.disney.studios.model;

import java.io.Serializable;

public class DogURL implements Serializable {
	private String url;
	
    public DogURL (String url) {
        this.url = url;
    }

	public void setURL(String url) {
		this.url = url;
	}
	public String getURL() {
		return this.url;
	}
}
