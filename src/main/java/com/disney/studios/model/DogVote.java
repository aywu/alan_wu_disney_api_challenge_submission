package com.disney.studios.model;

import java.io.Serializable;

public class DogVote implements Serializable {
    private static final long serialVersionUID = -7788619177798333712L;

    private String client_name;
	private int dog_pic_id;
	private boolean vote;
	
    public DogVote(String client_name, int dog_pic_id, boolean vote) {
        this.client_name = client_name;
        this.dog_pic_id = dog_pic_id;
        this.vote = vote;
    }

	public String getClientName() {
		return client_name;
	}
	public void setClientName(String client_name) {
		this.client_name = client_name;
	}
	public int getDogPicId() {
		return dog_pic_id;
	}
	public void setDogPicId(int dog_pic_id) {
		this.dog_pic_id = dog_pic_id;
	}
	public boolean getVoted() {
		return vote;
	}
	public void setVoted(boolean vote) {
		this.vote = vote;
	}
}
