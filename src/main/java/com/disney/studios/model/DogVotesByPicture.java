package com.disney.studios.model;

import java.io.Serializable;

public class DogVotesByPicture implements Serializable {
    private static final long serialVersionUID = -7788619177798333712L;

	private int dog_pic_id;
	private int count;
	
    public DogVotesByPicture(int dog_pic_id, int count) {
        this.dog_pic_id = dog_pic_id;
        this.count = count;
    }

	public int getDogPicId() {
		return dog_pic_id;
	}
	public void setDogPicId(int dog_pic_id) {
		this.dog_pic_id = dog_pic_id;
	}
	public int getCouunt() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
}
