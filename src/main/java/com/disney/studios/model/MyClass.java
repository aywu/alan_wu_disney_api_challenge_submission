package com.disney.studios.model;

import java.io.Serializable;
import java.util.ArrayList;

public class MyClass implements Serializable {
    private String name;
    private ArrayList<String> list = new ArrayList<String>();

    public MyClass(String name) {
        this.name = name;
        list.add(new String("str1"));
        list.add(new String("str2"));
        list.add(new String("str3"));
    } 
}
