package com.disney.studios.model;

import java.io.Serializable;

public class DogDetails implements Serializable {
	private String breed;
	private String url;
	
	public void setBreed(String thebreed) {
		breed = thebreed;
	}
	public void setURL(String theurl) {
		url = theurl;
	}

	public String getBreed() {
		return breed;
	}
	public String getURL() {
		return url;
	}
	
	public String toString() {
		return "Breed: " + breed + ", url: " + url;
	}
}
