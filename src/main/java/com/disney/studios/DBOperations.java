package com.disney.studios;

import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
// Mine
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import com.disney.studios.model.DogDetails;
import com.disney.studios.model.DogVotesByPicture;

import static org.slf4j.LoggerFactory.getLogger;




@Component("dbOperations")
public class DBOperations implements InitializingBean {
    private static final Logger LOGGER = getLogger(DBOperations.class);

    // DB
    Connection conn;

    @Autowired
    DataSource dataSource;

    public String toString() {
        return "DBOperations";
    }

    public boolean update(String sql) {
        boolean ret = true;
        Statement st = null;

        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }
            if (conn != null){
                System.out.println("Connection created successfully");
                st = conn.createStatement();    // statements
                int i = st.executeUpdate(sql);
                if (i != -1) {
                    System.out.println("Successfully executed: " + sql);
                } else {
                    ret = false;
                    System.out.println("Failed to execute: " + sql);
                }
            }else{
               ret = false;
               System.out.println("Problem with creating connection");
            }
        } catch (SQLException e) {
           ret = false;
           e.printStackTrace();
		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException exp) {
                ret = false;
		 		exp.printStackTrace();
			}
        }

        return ret;
    }

    public ArrayList<DogDetails> listDogDetails(String sql) {
        Statement st = null;
        ResultSet rs = null;
        ArrayList<DogDetails> result = new ArrayList<DogDetails>();

        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }

			st = conn.createStatement();
			rs = st.executeQuery(sql);
            DogDetails pic ;
            while (rs.next()) {
                pic = new DogDetails ();
                pic.setBreed(rs.getString("Breed"));
                pic.setURL(rs.getString("URL"));
                result.add(pic);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				
				if (st != null)
					st.close();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
		}
        
        System.out.println("query succeeded: " + sql);
        return result;
    }

    public ArrayList<DogVotesByPicture> listDogVotes(String sql) {
        Statement st = null;
        ResultSet rs = null;
        ArrayList<DogVotesByPicture> result = new ArrayList<DogVotesByPicture>();

        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }

			st = conn.createStatement();
			rs = st.executeQuery(sql);
            DogVotesByPicture votes ;
            while (rs.next()) {
                votes = new DogVotesByPicture(rs.getInt("dogid"), rs.getInt("Count"));
                result.add(votes);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				
				if (st != null)
					st.close();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
		}
        
        System.out.println("[listDogVotes()]: query succeeded: " + sql);
        return result;
    }

    public String queryBreedById(String sql) {
    	return queryOneStringColumn(sql, "Breed");
    }

    private String queryOneStringColumn(String sql, String colname) {
        Statement st = null;
        ResultSet rs = null;
        String result = "";

        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }

			st = conn.createStatement();
			rs = st.executeQuery(sql);
            if (rs.next()) {
                result = rs.getString(colname);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				
				if (st != null)
					st.close();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
		}
        
        System.out.println("[queryOneStringColumn()]: query succeeded: " + sql);
        return result;
    }

    public int queryVoteCountByDog(String sql) {
        return queryOneIntColumn(sql, "Count");
    }

    private int queryOneIntColumn(String sql, String colname) {
        Statement st = null;
        ResultSet rs = null;
        int result = 0;

        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }

			st = conn.createStatement();
			rs = st.executeQuery(sql);
            if (rs.next()) {
                result = rs.getInt(colname);
            }
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
				
				if (st != null)
					st.close();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
		}
        
        System.out.println("[queryOneIntColumn()]: query succeeded: " + sql);
        return result;
    }

    /**
     * Load the different breeds into the data source after
     * the application is ready.
     *
     * @throws Exception In case something goes wrong while we load the breeds.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
    }
}
