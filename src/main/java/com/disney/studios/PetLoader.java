package com.disney.studios;

import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
// Mine
import java.sql.Statement;
import java.sql.Connection;
import java.sql.SQLException;

import static org.slf4j.LoggerFactory.getLogger;


@Component("petLoader")
public class PetLoader implements InitializingBean {
    private static final Logger LOGGER = getLogger(PetLoader.class);

    private int mId;

    // DB
    Connection conn;

    // Resources to the different files we need to load.
    @Value("classpath:data/labrador.txt")
    private Resource labradors;

    @Value("classpath:data/pug.txt")
    private Resource pugs;

    @Value("classpath:data/retriever.txt")
    private Resource retrievers;

    @Value("classpath:data/yorkie.txt")
    private Resource yorkies;

    @Autowired
    DataSource dataSource;

    public String toString() {
        return "PetLoad";
    }

    private void update(String sql) {
        Statement st = null;
        try {
            if (conn == null) {
                conn = dataSource.getConnection();
            }
            if (conn != null){
                System.out.println("Connection created successfully");
                st = conn.createStatement();    // statements
                int i = st.executeUpdate(sql);
                if (i != -1) {
                    System.out.println("Successfully executed: " + sql);
                } else {
                    System.out.println("Failed to execute: " + sql);
                }
            }else{
               System.out.println("Problem with creating connection");
            }
        } catch (SQLException e) {
            e.printStackTrace();
		} finally {
			try {
				if (st != null)
					st.close();
			} catch (SQLException exp) {
				exp.printStackTrace();
			}
        }
    }

    private void createTables() {
        update("CREATE OR REPLACE TABLE DOGS(id INTEGER PRIMARY KEY, Breed VARCHAR(200), URL VARCHAR(1000))");
        update("CREATE OR REPLACE TABLE VOTE(client_name VARCHAR(200) NOT NULL, dogid INTEGER NOT NULL, voted BOOL, PRIMARY KEY(client_name, dogid), FOREIGN KEY(dogid) REFERENCES DOGS(id))");
    }

    private void insertURL(int id, String breed, String url) {
        update("INSERT INTO DOGS(id, Breed, URL) VALUES (" + id + ", '" + breed + "', '" + url + "')");
    }

    /**
     * Load the different breeds into the data source after
     * the application is ready.
     *
     * @throws Exception In case something goes wrong while we load the breeds.
     */
    @Override
    public void afterPropertiesSet() throws Exception {
        createTables();

        loadBreed("Labrador", labradors);
        loadBreed("Pug", pugs);
        loadBreed("Retriever", retrievers);
        loadBreed("Yorkie", yorkies);
    }

    /**
     * Reads the list of dogs in a category and (eventually) add
     * them to the data source.
     * @param breed The breed that we are loading.
     * @param source The file holding the breeds.
     * @throws IOException In case things go horribly, horribly wrong.
     */
    private void loadBreed(String breed, Resource source) throws IOException {
        LOGGER.debug("Loading breed {}", breed);
        try ( BufferedReader br = new BufferedReader(new InputStreamReader(source.getInputStream()))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("loadBreed: " + line);
                LOGGER.debug(line);
                insertURL(mId++, breed, line);
            }
        }
    }
}
