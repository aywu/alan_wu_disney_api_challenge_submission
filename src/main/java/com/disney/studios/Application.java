package com.disney.studios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class Application {
    public static DBOperations dbBean;

    public static void main(String[] args) {
    		ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);
    		dbBean = (DBOperations) applicationContext.getBean("dbOperations");
    		PetLoader pets = (PetLoader) applicationContext.getBean("petLoader");
    }
}
