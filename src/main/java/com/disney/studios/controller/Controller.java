package com.disney.studios.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
import java.io.ByteArrayOutputStream;

import com.disney.studios.model.DogURL;
import com.disney.studios.model.DogVote;
import com.disney.studios.model.DogDetails;
import com.disney.studios.model.MyClass;
import com.disney.studios.model.DogVotesByPicture;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.disney.studios.Application;
import com.disney.studios.DBOperations;


@RestController
public class Controller
{
	@RequestMapping("/")
	public String test() {
		return "Test";
	}

	@RequestMapping("/test")
	public String test2() {
        ArrayList<MyClass> mylist = new ArrayList<MyClass>();
        MyClass obj1 = new MyClass("collection1");
        mylist.add(obj1);
       
        return convertToJsonAll(mylist);
	}
	
	@RequestMapping("/pictures/all")
	public String pictures_all() {
        String sql = "SELECT Breed, URL FROM DOGS ORDER BY Breed"; 
        ArrayList<DogDetails> list = Application.dbBean.listDogDetails(sql);
        ArrayList<DogURL> list2 = new ArrayList<DogURL>();

        if (list.size() > 0) {
            DogURL pic;
            for (DogDetails detail : list) {
            	pic = new DogURL(detail.getURL());
                list2.add(pic);
            }
        	return convertToJsonAll(list2);
        } else {
		    return "[{\"Message\":\"No dog picture available.\"}]";
        }
	}

	@RequestMapping(value = "/pictures/{breed}", method=RequestMethod.GET)
	public @ResponseBody String getBreedById(@PathVariable("breed") String breed) {
        String sql = "SELECT URL, Breed FROM DOGS WHERE Breed = '" + breed + "'";
        ArrayList<DogDetails> list = Application.dbBean.listDogDetails(sql);

        ArrayList<DogURL> list2 = new ArrayList<DogURL>();

        if (list.size() > 0) {
            DogURL url;
            for (DogDetails detail : list) {
            	url = new DogURL(detail.getURL());
                list2.add(url);
            }
        	return convertToJsonAll(list2);
        } else {
		    return "{\"Message\":\"No dog picture for breed " + breed + ".\"}";
        }
	}

	@RequestMapping(value = "/details/{dog_pic_id}", method=RequestMethod.GET)
	public @ResponseBody String getDetailsById(@PathVariable("dog_pic_id") String dog_pic_id) {
        String sql = "SELECT URL, Breed FROM DOGS WHERE id = '" + dog_pic_id + "'";
        ArrayList<DogDetails> list = Application.dbBean.listDogDetails(sql);

        if (list != null && list.size() > 0) {
        	return convertToJsonAll(list);
        } else {
		    return "{\"Message\":\"No dog picture for dog picture " + dog_pic_id + ".\"}";
        }
	}

	@RequestMapping(value = "/breed/{dog_pic_id}", method=RequestMethod.GET)
	public @ResponseBody String getBreedById(@PathVariable("dog_pic_id") int dog_id) {
		DBOperations db = Application.dbBean;
        String sql = "SELECT Breed FROM DOGS WHERE id = " + dog_id;
        String result = "{\"breed\": \"" + Application.dbBean.queryBreedById(sql) + "\"}";
        return result;
	}

	@RequestMapping(value = "/vote/dog", method=RequestMethod.POST)
	public @ResponseBody String vote(@RequestBody DogVote vote) {
        String client_name = vote.getClientName();
        int dog_pic_id = vote.getDogPicId();
        boolean voted =  vote.getVoted();
        String sql = "INSERT INTO VOTE(client_name, dogid, voted) VALUES ('" + client_name + "', " +  dog_pic_id + ", " + voted + ")";
        if (Application.dbBean.update(sql)) {
            return "Succeeded to save voting (client, id, vote) = (" + client_name + "," + dog_pic_id + "," + voted + ")";
        } else {
            return "Failed to save voting (client, id, vote) = (" + client_name + "," + dog_pic_id + "," + voted + ")";
        }
	}
	
	@RequestMapping(value = "/vote/{dog_pic_id}", method=RequestMethod.GET)
	public @ResponseBody String getVoteCountById(@PathVariable("dog_pic_id") int dog_pic_id) {
        String sql = "SELECT COUNT(*) Count FROM VOTE WHERE dogid = " + dog_pic_id;
        int count = Application.dbBean.queryVoteCountByDog(sql);
        return "{\"dog_pic_id\":" + dog_pic_id + ", \"Vote_Count\":" + count + "}";
	}

	@RequestMapping(value = "/all_votes", method=RequestMethod.GET)
	public @ResponseBody String getVotesGroupByPicture() {
        String sql = "SELECT COUNT(*) Count, dogid FROM VOTE GROUP BY dogid ORDER BY Count DESC";
        ArrayList<DogVotesByPicture> list = Application.dbBean.listDogVotes(sql);   
        if (list.size() > 0) {
        	return convertToJsonAll(list);
        } else {
		    return "{\"Message\":\"Nobody voted.\"}";
        }
	}

    // Group the number of votes by dog picture for all votes greater than {10}. 
	@RequestMapping(value = "/votes/{min_limit}", method=RequestMethod.GET)
	public @ResponseBody String getNumbersOfVotesGroupByDogPictures(@PathVariable("min_limit") int min_limit) {
        String sql = "SELECT COUNT(*) Count, dogid FROM VOTE GROUP BY dogid HAVING Count > " + min_limit;
        ArrayList<DogVotesByPicture> list = Application.dbBean.listDogVotes(sql); 
        if (list.size() > 0) {
        	return convertToJsonAll(list);
        } else {
		    return "{\"Message\":\"No pictures received votes more than " + min_limit + ".\"}";
        }
	}

	private void printList(ArrayList<DogURL> list) {
		for (DogURL p: list) {
			System.out.print(p);
		}
	}
	
    private <T> String convertToJsonAll(ArrayList<T> list) {
        final ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        final ObjectMapper mapper = new ObjectMapper();
        String json = "";
        
        try {
			mapper.writeValue(outStream, list);
			json = new String(outStream.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}

        return json;
    }
}
